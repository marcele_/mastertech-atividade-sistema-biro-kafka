package br.com.mastertech.cnpj.consumers;

import br.com.mastertech.cnpj.models.Capital;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class LogEmpresasConsumer {

    @KafkaListener(topics = "spec3-marcele-menezes-3", groupId = "MarceleMF-1")

    public void receber(@Payload Capital capital) throws Exception {
        /*System.out.println("Recebi a empresa (CNPJ): " + capital.getCnpj() + ", Nome: " + capital.getNome()
                + ", Capital: " + capital.getCapitalSocial().doubleValue()
                + ", Cadastro: " + capital.isCadastroAceito());*/
        salvarArquivoCSV(capital);
    }

    public void salvarArquivoCSV(Capital capital) throws IOException,
            CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        Writer writer;
        if (capital.isCadastroAceito() == true) {
            writer = new FileWriter("/home/a2w/Documentos/mastertech especializacao/atividades/atividade-sistema-biro-kafka/cadastro-capital-valido.csv", true);
            System.out.println("SALVOU A EMPRESA (CNPJ): " + capital.getCnpj() + ", Nome: " + capital.getNome()
                    + ", Capital: " + capital.getCapitalSocial().doubleValue()
                    + ", Cadastro: " + capital.isCadastroAceito());
        } else {
            writer = new FileWriter("/home/a2w/Documentos/mastertech especializacao/atividades/atividade-sistema-biro-kafka/cadastro-capital-recusado.csv", true);
            System.out.println("SALVOU A EMPRESA (CNPJ): " + capital.getCnpj() + ", Nome: " + capital.getNome()
                    + ", Capital: " + capital.getCapitalSocial().doubleValue()
                    + ", Cadastro: " + capital.isCadastroAceito());
        }

        StatefulBeanToCsv<Capital> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(capital);

        writer.flush();
        writer.close();

    }

}
