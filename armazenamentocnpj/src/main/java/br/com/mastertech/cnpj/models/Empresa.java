package br.com.mastertech.cnpj.models;

public class Empresa {

    private long cnpj;

    public Empresa() {
    }

    public long getCnpj() {
        return cnpj;
    }

    public void setCnpj(long cnpj) {
        this.cnpj = cnpj;
    }
}
