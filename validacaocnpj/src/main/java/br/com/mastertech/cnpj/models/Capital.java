package br.com.mastertech.cnpj.models;

import java.math.BigDecimal;

public class Capital {

    private String cnpj;
    private String nome;
    private BigDecimal capitalSocial;
    private boolean cadastroAceito;

    public Capital() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(BigDecimal capitalSocial) {
        this.capitalSocial = capitalSocial;
    }

    public boolean isCadastroAceito() {
        return cadastroAceito;
    }

    public void setCadastroAceito(boolean cadastroAceito) {
        this.cadastroAceito = cadastroAceito;
    }
}
