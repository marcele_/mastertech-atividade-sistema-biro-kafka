package br.com.mastertech.cnpj.consumers;

import br.com.mastertech.cnpj.models.Empresa;
import br.com.mastertech.cnpj.services.ValidacaocnpjService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class KafkaEmpresaConsumer {

    @Autowired
    private ValidacaocnpjService validacaocnpjService;

    @KafkaListener(topics = "spec3-marcele-menezes-2", groupId = "MarceleMF-1")
    public void receber(@Payload Empresa empresa) throws Exception {
        validacaocnpjService.validarCnpj(empresa);
    }

}
