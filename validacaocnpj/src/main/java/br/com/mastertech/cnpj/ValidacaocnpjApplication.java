package br.com.mastertech.cnpj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ValidacaocnpjApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidacaocnpjApplication.class, args);
	}

}
