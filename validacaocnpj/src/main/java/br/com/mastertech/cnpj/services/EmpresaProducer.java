package br.com.mastertech.cnpj.services;

import br.com.mastertech.cnpj.models.Capital;
import br.com.mastertech.cnpj.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {

    @Autowired
    private KafkaTemplate<String, Capital> producer;

    public void enviarAoKafka(Capital capital) {
        System.out.println("2º MS Enviando - Empresa (CNPJ): " + capital.getCnpj() + ", Nome: " + capital.getNome()
                + ", Capital: " + capital.getCapitalSocial().doubleValue()
                + ", Cadastro: " + capital.isCadastroAceito());
        producer.send("spec3-marcele-menezes-3", capital);
    }

}
