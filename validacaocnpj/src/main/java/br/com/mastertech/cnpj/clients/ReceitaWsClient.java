package br.com.mastertech.cnpj.clients;

import br.com.mastertech.cnpj.clients.dtos.ReceitaWsResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "receitaws", url = "https://www.receitaws.com.br/v1/cnpj")
public interface ReceitaWsClient {

    @GetMapping("/{cnpj}")
    Optional<ReceitaWsResponse> consultaCNPJ(@PathVariable long cnpj);

}
