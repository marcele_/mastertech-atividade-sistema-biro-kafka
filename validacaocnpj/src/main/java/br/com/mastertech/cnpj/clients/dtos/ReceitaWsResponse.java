package br.com.mastertech.cnpj.clients.dtos;

import java.math.BigDecimal;

public class ReceitaWsResponse {

    private String nome;
    public BigDecimal capital_social;

    public ReceitaWsResponse(){}

    public BigDecimal getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(BigDecimal capital_social) {
        this.capital_social = capital_social;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
