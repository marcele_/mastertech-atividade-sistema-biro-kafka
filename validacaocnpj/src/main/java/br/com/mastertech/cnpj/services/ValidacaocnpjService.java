package br.com.mastertech.cnpj.services;

import br.com.mastertech.cnpj.clients.ReceitaWsClient;
import br.com.mastertech.cnpj.clients.dtos.ReceitaWsResponse;
import br.com.mastertech.cnpj.models.Capital;
import br.com.mastertech.cnpj.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ValidacaocnpjService {

    @Autowired
    private ReceitaWsClient receitaWsClient;

    @Autowired
    private EmpresaProducer empresaProducer;

    public void validarCnpj(Empresa empresa) {

        try {
            if (!empresa.getCnpj().isEmpty()) {
                long cnpj = Long.parseLong(empresa.getCnpj());
                Optional<ReceitaWsResponse> receitaWsResponseOptional =
                        receitaWsClient.consultaCNPJ(cnpj);

                if (receitaWsResponseOptional.isPresent()) {
                    Capital capital = new Capital();
                    capital.setCnpj(empresa.getCnpj());
                    capital.setNome(receitaWsResponseOptional.get().getNome());
                    capital.setCapitalSocial(receitaWsResponseOptional.get().getCapital_social());

                    //if (receitaWsResponseOptional.get().getCapital_social().doubleValue() > 1000000.00) {
                    if (capital.getCapitalSocial().doubleValue() > 1000000.00) {
                        capital.setCadastroAceito(true);
                        empresaProducer.enviarAoKafka(capital);
                    } else{
                        capital.setCadastroAceito(false);
                        empresaProducer.enviarAoKafka(capital);
                    }

                }
            } else {
                System.out.println("Não recebi nenhum CNPJ: "+empresa.getCnpj());
            }

        } catch (Exception e) {
            System.out.println("ERRO: "+ e.getMessage());
        }

    }
}
