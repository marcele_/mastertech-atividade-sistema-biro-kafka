package br.com.mastertech.cnpj.controllers;

import br.com.mastertech.cnpj.models.Empresa;
import br.com.mastertech.cnpj.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void cadastrar(@RequestBody Empresa empresa){
        empresaService.cadastrar(empresa);
    }

}
