package br.com.mastertech.cnpj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadastrocnpjApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastrocnpjApplication.class, args);
	}

}
