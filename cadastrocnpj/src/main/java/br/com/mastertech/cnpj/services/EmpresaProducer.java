package br.com.mastertech.cnpj.services;

import br.com.mastertech.cnpj.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public void enviarKafka(Empresa empresa) {
        System.out.println("1º MS - Empresa (CNPJ): "+ empresa.getCnpj());
        producer.send("spec3-marcele-menezes-2", empresa);
    }
}
