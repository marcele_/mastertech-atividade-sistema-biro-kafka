package br.com.mastertech.cnpj.services;

import br.com.mastertech.cnpj.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    @Autowired
    private EmpresaProducer empresaProducer;

    public void cadastrar(Empresa empresa){
        empresaProducer.enviarKafka(empresa);
    }
}
